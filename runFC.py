#!/usr/local/bin/python

import PAM.hamNonCorrel as ham
import PAM.qs as qs
import numpy as np
from scipy.optimize import root
import sys
import random as ran
from copy import copy
import datetime as dt
from os.path import isfile as isfile

zero_tol = 1e-10

def err(*args):
  for a in args:
    sys.stderr.write(str(a)+' ')
    sys.stderr.write('\n')

if len(sys.argv)<8:
  print "No arguments!"
  print "Lambda, n, h, V, U, phaseIndicator, ef:"
  print '\t\t\t\t(0) Paramagnetic insulator\n\
	 \t\t\t(1) Paramagnetic metal\n\
	 \t\t\t(2) Ferrimagnetic insulator (U)\n\
	 \t\t\t(3) Ferrimagnetic metal (U)\n\
	 \t\t\t(4) Ferrimagnetic insulator (D)\n\
	 \t\t\t(5) Ferrimagnetic metal (D)\n\
	 \t\t\t(6) Ferromagnetic insulator (U)\n\
	 \t\t\t(7) Ferromagnetic insulator (D)'
  exit(-1)

Lambda=int(sys.argv[1])

kxMax = np.pi
kyMax = np.pi
kxs = np.linspace(-kxMax,kxMax,Lambda)
kys = np.linspace(-kyMax,kyMax,Lambda)
kxxs,kyys = np.meshgrid(kxs,kys)

ks = np.stack((np.ravel(kxxs), np.ravel(kyys)), axis=-1)

def eps(ts,ks): # SC lattice with only 1st, 2nd, and 3rd NN hopping
  return 2*ts[0]*(np.cos(  ks[:,0])+np.cos(  ks[:,1])) \
       + 4*ts[1]* np.cos(  ks[:,0])*np.cos(  ks[:,1])  \
#       + 2*ts[2]*(np.cos(2*ks[:,0])+np.cos(2*ks[:,1]))

n  = float(sys.argv[2]) 
#ef = -3.
ef = float(sys.argv[7])
h  = float(sys.argv[3])
V  = float(sys.argv[4])
Uf = float(sys.argv[5])
e = int(sys.argv[6])

my_name = ".rmb"
for t in sys.argv[2:]: # omija Lambda!
  my_name += "_"
  my_name += t

epsSC = ham.DispRel(eps=eps,BZone=ks,t=np.array([-1.0,0.25,0.0625]))
PAM = ham.FreeElectron(params=[n,ef,V,Uf,h],beta=1000.0,eps=epsSC)

def start(n,m,d):
  return np.array([n,m,d,np.sqrt(np.abs(2-2*n+2*d*d)),np.sqrt(np.abs(n-m-2*d*d)),np.sqrt(np.abs(n+m-2*d*d)),-0.1,-0.1,-0.1,0.1,0.1,ef])

stats = [ start(0.9, 0.0,0.0),
          start(0.9, 0.0,0.4),
          start(0.9, 0.7,0.0),
          start(0.9, 0.7,0.4),
          start(0.9,-0.7,0.0),
          start(0.9,-0.7,0.4),
          start(0.9, 0.88,0.0),
          start(0.9,-0.88,0.0)]
comms = [ '\n(0) Paramagnetic insulator',
	  '\n(1) Paramagnetic metal',
	  '\n(2) Ferrimagnetic insulator (U)',
	  '\n(3) Ferrimagnetic metal (U)',
	  '\n(4) Ferrimagnetic insulator (D)',
	  '\n(5) Ferrimagnetic metal (D)',
	  '\n(6) Ferromagnetic insulator (U)',
	  '\n(7) Ferromagnetic insulator (D)']
conds = [ [[1,'e'],[2,'e']],
          [[1,'e'],[2,'g']],
          [[1,'g'],[2,'e']],
          [[1,'g'],[2,'g']],
          [[1,'l'],[2,'e']],
          [[1,'l'],[2,'g']],
          [[1,'g'],[2,'e']],
          [[1,'l'],[2,'e']] ]

MAX = int(1e4)

def list_satisfy_condition(lst,c):
  if c[1] == 'e':
    return np.abs(lst[c[0]]) < zero_tol 
  elif c[1] == 'g':
    return lst[c[0]] > zero_tol
  elif c[1] == 'l':
    return lst[c[0]] <-zero_tol

with open("result_%d_with_L=%d_n=%.2f_h=%f_V=%.2f_Uf=%.1f_ef=%.1f.dat"%(e,Lambda,n,h,V,Uf,ef),"a+") as fil:
  fil.write("#\n# New run @ %s\n"%str(dt.datetime.now())[:-7])
  fil.write("#\n# Aiming @ %s\n"%comms[e][5:]),
  fil.write("#\n# phase_id beta temperature n h V Uf ef nf m mf df mU mD FreeEnergy error\n#\n")

#met = 'lm'
met = 'hybr'
#met = 'broyden1'
dbeta = (np.power(2.0,-0.5) - 1.0)/(1.0 - np.power(2.0,0.5))
first_run = True

# reading from file if was run before
p0 = np.array([])
if isfile(my_name):
  p0 = np.loadtxt(my_name)
if len(p0) != len(stats[e]):
  p0 = copy(stats[e])
else:
  first_run = False

for i in range(-12,6): 
  beta = 11604.5221105*np.power(2.,0.5*i)

  PAM.beta = beta
  err("Runs in T = %f K"%np.power(2.,-0.5*i))
  com = comms[e]
  cond = conds[e]
  tst = False
  err(com)

  for num_of_iters in range(MAX):
    err("Trial run @ %.5f %.5f %.5f"%(p0[0],p0[1],p0[2]))
    #sol = root(PAM,p0,method=met,tol=1e-10,options={'maxiter': 50000})
    sol = root(PAM,p0,method=met,tol=zero_tol)
    tst = sol['success']
    er  = PAM.err(sol['x'])

    mf  = sol['x'][1]
    df  = sol['x'][2]

    if not tst:
      err(sol['message'])
    elif er > np.sqrt(zero_tol):
      err("Failed with 0 != %.3e"%er)
      tst = False

    for c in cond:
      if not list_satisfy_condition(sol['x'],c):
        err("Failed with m = %.3e & d == %.3e"%(mf,df))
        tst = False

    if tst:
      break

    if p0[2] < 2*zero_tol:
      p0[2] = 0
      choice = ran.choice(range(2))
    else:
      choice = ran.choice(range(3))

    if choice == 0:
      p0[0] += 0.2*ran.gauss(0.0,np.exp(-0.2*MAX)+0.01)
      if p0[0] < 0.01:
        p0[0] = 0.2
    elif choice == 1:
      p0[1] += 0.2*(np.abs(p0[1])+1e-6)*np.abs(ran.gauss(0.0,np.exp(-0.2*MAX)+0.1))
      if np.abs(p0[1]) > p0[0]:
	p0[1]*=0.25
    elif choice == 2:
      change = 0.1*(np.abs(p0[2])+1e-6)*ran.gauss(0.0,np.exp(-0.2*MAX)+0.01)
      if p0[2]+change > 0:
        p0[2] += change

  if not tst:
    err("Did not converge!")
    with open("result_%d_with_L=%d_n=%.2f_h=%f_V=%.2f_Uf=%.1f_ef=%.1f.dat"%(e,Lambda,n,h,V,Uf,ef),"a+") as fil:
      fil.write("# No convergence @ %d %f %f \n"%(e, Lambda, beta))
    break

  nf  = sol['x'][0]
  mf  = sol['x'][1]
  df  = sol['x'][2]
  f0  = sol['x'][3]
  fu  = sol['x'][4]
  fd  = sol['x'][5]
  mu  = sol['x'][11]
  m   = PAM.m(sol['x'])
  mc=m-mf
  nc=n-nf
  qU = qs.SqUP(nf,mf,df,f0,fu,fd)
  qD = qs.SqDO(nf,mf,df,f0,fu,fd)

  if (np.abs(qU) < 1e-12):
    mU = 1e12
  else:
    mU = np.abs(1./qU)
  if (np.abs(qD) < 1e-12):
    mD = 1e12
  else:
    mD = np.abs(1./qD)

  FreeEnergy = PAM.Landau(sol['x'],mu='')
  with open("result_%d_with_L=%d_n=%.2f_h=%f_V=%.2f_Uf=%.1f_ef=%.1f.dat"%(e,Lambda,n,h,V,Uf,ef),"a+") as fil:
    fil.write("%d %d %f %f %f %f %f %f %f %e %e %e %e %e %e %e %e\n"%(e, Lambda, beta, np.power(2.,-0.5*i), n, h, V, Uf, ef, nf, m, mf, df, mU, mD, FreeEnergy, er))
  print e, Lambda, beta, np.power(2.,-0.5*i), n, h, V, Uf, ef, nf, m, mf, df, mU, mD, FreeEnergy, er
  if first_run:
    p0 = sol['x']
    np.savetxt(my_name,p0)
    first_run = False
  p0 += dbeta*(sol['x']-p0)

