#ifndef HAMILTONIAN_H
#define HAMILTONIAN_H

#include <iostream>
#include <fstream>
#include <vector>
#include <array>

#include <cmath>

#include <omp.h>

#include "auxiliary.h"

namespace sga{

class PeriodicAnderson1p1{
protected:
  size_t N;
  double dk;
  DispRelation<2> dispersion;
  double n,ef,V,Uf,h;
  std::vector<double> masses;
  double beta;
  BandNarrowingFactors<double> qs;
public:
  std::vector<double> fields;

  PeriodicAnderson1p1();
  double singe_particle_energy(double ek, const std::vector<double>& fields, int spin=1, int eigenSplitter=1);
  double log_exp(double E) const; 
  double Fermi_Dirac(double E) const; 

  double Landau(const std::vector<double>& fields);
  double free_energy(const std::vector<double>& fields);
  std::vector<double> system_of_equations(const std::vector<double>& fields);

  void BZ() const;
  void BZ(const std::vector<double>& fields);
  void BZ(const std::vector<double>& fields, const std::string& filename);

  std::vector<double> get_masses() const;

  double set_n   (const double&  _n);
  double set_n   (const double&& _n);
  double set_ef  (const double&  _ef);
  double set_ef  (const double&& _ef);
  double set_V   (const double&  _V);
  double set_V   (const double&& _V);
  double set_Uf  (const double&  _Uf);
  double set_Uf  (const double&& _Uf);
  double set_h   (const double&  _h);
  double set_h   (const double&& _h);
  double set_beta(const double&  _beta);
  double set_beta(const double&& _beta);
  size_t    set_N(const size_t&  _N);
  size_t    set_N(const size_t&& _N);
}; //end of class PeriodicAnderson1p1

} //end of namespace sga
#endif
