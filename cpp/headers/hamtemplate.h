#ifndef HAMILTONIAN_H
#define HAMILTONIAN_H

#include <iostream>
#include <fstream>
#include <vector>
#include <array>

#include <cmath>

#include <omp.h>

namespace sga{

enum StandardEnum { n, m, d, f0, fUP, fDO, ln, lm, mu, l0, lUP, lDO };

template<typename T, class SingleParticleSystem,
                     class BandNarrowingFactors,
                     class DispRelation,
                     typename fieldsNames=StandardEnum,
                     size_t numberOfDistinguishableParticles = 4,
                     size_t dim = 2>
class AndersonHamiltonian{
protected:
  T n,ef,V,Uf;                        // Hamiltonian params
  size_t N;                           // amount of k-space points IN ONE DIRECTION
  T beta;                             // reverse temperature (in energy units)
  T h,SOC;                            // Hamiltonian params
public:
  DispRelation dispersion;            // relation of dispersion for free electron
protected:
  T dk;                               // step in k-space
  std::vector<T> masses;              // f-electrons effective mass
  BandNarrowingFactors qs;            // band narrowing factors
  SingleParticleSystem freeElectrons; // energetics of SP-system

public:
  std::vector<T> fields;

  AndersonHamiltonian(const T&            _n    , 
                      const T&            _ef   , 
                      const T&            _V    , 
                      const T&            _Uf   , 
                      const size_t&       _N    , 
                      const T&            _beta , 
                      const T&            _h    , 
                      const T&            _SOC  , 
                      const DispRelation& _disp ):n(_n), 
                                                  ef(_ef),
                                                  V(_V), 
                                                  Uf(_Uf), 
                                                  N(_N), 
                                                  beta(_beta), 
                                                  h(_h), 
                                                  SOC(_SOC), 
                                                  dispersion(_disp), 
                                                  freeElectrons(ef,V,h,SOC){ 
  dk = 2*M_PI/(N-1);
  masses = std::vector<T>(2,0.0);
}

  AndersonHamiltonian(const T&&            _n    =  1.5, 
                      const T&&            _ef   = -3.0, 
                      const T&&            _V    = -1.0, 
                      const T&&            _Uf   =  8.0, 
                      const size_t&&       _N    =  256, 
                      const T&&            _beta =  256, 
                      const T&&            _h    =  0.0, 
                      const T&&            _SOC  =  0.01, 
                      const DispRelation&& _disp = DispRelation()):n(_n), 
                                                                   ef(_ef),
                                                                   V(_V), 
                                                                   Uf(_Uf), 
                                                                   N(_N), 
                                                                   beta(_beta), 
                                                                   h(_h), 
                                                                   SOC(_SOC), 
                                                                   dispersion(_disp),
								   freeElectrons(ef,V,h,SOC){ 
  dk = 2*M_PI/(N-1);
  masses = std::vector<T>(2,0.0);
}

  T log_exp(T E) const{
  if(E<0)
    return E-log(1.0 + exp(beta*E))/beta; 
  else
    return -log(1.0 + exp(-beta*E))/beta; 
}
 
  T Fermi_Dirac(T E) const{ 
  return 0.5*(1.0 - tanh(0.5*beta*E));
}

  T Landau(const std::vector<T>& fields){
  auto E = free_energy(fields);
  E -= fields[fieldsNames::mu]*n;

  return E;
}

  void get_k_point(const size_t& i, std::array<T,dim>& k) const{
    k[0] = M_PI*dk*(i%N) - M_PI;

    for(size_t i=1; i<dim; ++i)
      k[i] = M_PI*dk*(i/static_cast<size_t>(std::pow(N,i))) - M_PI;
  }

  T free_energy(const std::vector<T>& fields){
  freeElectrons.set_fields(fields);
  auto qfUP  = qs.narrow_UP(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDO  = qs.narrow_DO(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  T E  = 0.0;
  std::array<T,dim> k;
  #pragma omp parallel for reduction (+:E)
  for(unsigned i=0; i<static_cast<size_t>(pow(N,dim)); ++i){
    get_k_point(i,k);
    T ek = dispersion(k);
    for(unsigned j=0; j<numberOfDistinguishableParticles; ++j)
      E += log_exp(freeElectrons.energy(ek,qfUP,qfDO,j));
  }

  E /= static_cast<size_t>(pow(N,dim));

  E += fields[fieldsNames::ln]*fields[fieldsNames::n] + fields[fieldsNames::lm]*fields[fieldsNames::m];
  E += Uf*fields[fieldsNames::d]*fields[fieldsNames::d];
  E += fields[fieldsNames::l0]*(fields[fieldsNames::f0]*fields[fieldsNames::f0] - 2. + 2*fields[fieldsNames::n] - 2*fields[fieldsNames::d]*fields[fieldsNames::d]);
  E += fields[fieldsNames::lUP]*(fields[fieldsNames::fUP]*fields[fieldsNames::fUP] - fields[fieldsNames::n] - fields[fieldsNames::m] + 2*fields[fieldsNames::d]*fields[fieldsNames::d]);
  E += fields[fieldsNames::lDO]*(fields[fieldsNames::fDO]*fields[fieldsNames::fDO] - fields[fieldsNames::n] + fields[fieldsNames::m] + 2*fields[fieldsNames::d]*fields[fieldsNames::d]);

  E += fields[fieldsNames::mu]*n;

  return E;
} 

std::vector<T> system_of_equations(const std::vector<T>& fields){
  freeElectrons.set_fields(fields);
  std::vector<T> system(12,0.0);
  T Xn  = 0.0;
  T Xm  = 0.0;
  T Xd  = 0.0;
  T Xf0 = 0.0;
  T Xfu = 0.0;
  T Xfd = 0.0;
  T Xmu = 0.0;
  T Xln = 0.0;
  T Xlm = 0.0;

std::cout<<1<<std::endl;

  auto qfUP  = qs.narrow_UP(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDO  = qs.narrow_DO(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<2<<std::endl;
  auto qfUPdn  = qs.narrow_UP_dn(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdn  = qs.narrow_DO_dn(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<3<<std::endl;
  auto qfUPdm  = qs.narrow_UP_dm(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdm  = qs.narrow_DO_dm(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<4<<std::endl;
  auto qfUPdd  = qs.narrow_UP_dd(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdd  = qs.narrow_DO_dd(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<5<<std::endl;
  auto qfUPdf0  = qs.narrow_UP_df0(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdf0  = qs.narrow_DO_df0(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<6<<std::endl;
  auto qfUPdfUP  = qs.narrow_UP_dfUP(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdfUP  = qs.narrow_DO_dfUP(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<7<<std::endl;
  auto qfUPdfDO  = qs.narrow_UP_dfDO(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);
  auto qfDOdfDO  = qs.narrow_DO_dfDO(fields[fieldsNames::n],fields[fieldsNames::m],fields[fieldsNames::d],fields[fieldsNames::f0],fields[fieldsNames::fUP],fields[fieldsNames::fDO]);

std::cout<<8<<std::endl;
  std::array<T,dim> k;
  #pragma omp parallel for private(k) reduction (+:Xn,Xm,Xd,Xf0,Xfu,Xfd,Xmu,Xln,Xlm)
  for(unsigned i=0; i<static_cast<size_t>(pow(N,dim)); ++i){
    get_k_point(i,k);
std::cout<<9<<std::endl;
    T ek = dispersion(k);

std::cout<<10<<std::endl;
    for(unsigned j=0; j<numberOfDistinguishableParticles; ++j){
std::cout<<11<<std::endl;
      auto E = Fermi_Dirac(freeElectrons.energy(ek,fields,j));
      Xn  += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfUPdn);
      Xn  += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfDOdn);
      Xm  += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfUPdm);
      Xm  += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfUPdm);
      Xd  += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfDOdd);
      Xd  += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfDOdd);
      Xf0 += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfDOdf0);
      Xf0 += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfDOdf0);
      Xfu += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfDOdfUP);
      Xfu += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfDOdfUP);
      Xfd += E*(freeElectrons.d_energy_dQU(ek,qfUP,qfDO,j)*qfDOdfDO);
      Xfd += E*(freeElectrons.d_energy_dQD(ek,qfUP,qfDO,j)*qfDOdfDO);
      Xmu += E*(freeElectrons.d_energy_dmu(ek,qfUP,qfDO,j));
      Xln += E*(freeElectrons.d_energy_dln(ek,qfUP,qfDO,j));
      Xlm += E*(freeElectrons.d_energy_dlm(ek,qfUP,qfDO,j));
std::cout<<12<<std::endl;
    }
  }

std::cout<<13<<std::endl;
  Xn  /= std::pow(N,dim);
  Xm  /= std::pow(N,dim);
  Xd  /= std::pow(N,dim);
  Xf0 /= std::pow(N,dim);
  Xfu /= std::pow(N,dim);
  Xfd /= std::pow(N,dim);
  Xmu /= std::pow(N,dim);
  Xln /= std::pow(N,dim);
  Xlm /= std::pow(N,dim);
  
std::cout<<14<<std::endl;
  system[fieldsNames::n] = Xn + fields[fieldsNames::ln] + 2*fields[fieldsNames::l0] - fields[fieldsNames::lUP] - fields[fieldsNames::lDO]; 
  system[fieldsNames::m] = Xm + fields[fieldsNames::lm] - fields[fieldsNames::lUP] + fields[fieldsNames::lDO];
  system[fieldsNames::d] = Xd + 2*fields[fieldsNames::d]*(Uf + 2*(-fields[fieldsNames::l0] + fields[fieldsNames::lUP] + fields[fieldsNames::lDO]));
  system[fieldsNames::f0] = Xf0 + 2*fields[fieldsNames::f0]*fields[fieldsNames::l0];
  system[fieldsNames::fUP] = Xfu + 2*fields[fieldsNames::fUP]*fields[fieldsNames::lUP];
  system[fieldsNames::fDO] = Xfd + 2*fields[fieldsNames::fDO]*fields[fieldsNames::lDO];
  system[fieldsNames::ln] = Xln + fields[fieldsNames::n];
  system[fieldsNames::lm]= Xlm + fields[fieldsNames::m];
  system[fieldsNames::mu]= Xmu - n;
  system[fieldsNames::l0] = fields[fieldsNames::f0]*fields[fieldsNames::f0] - 2. + 2*fields[fieldsNames::n] - 2*fields[fieldsNames::d]*fields[fieldsNames::d];
  system[fieldsNames::lUP] = fields[fieldsNames::fUP]*fields[fieldsNames::fUP] - fields[fieldsNames::n] - fields[fieldsNames::m] + 2*fields[fieldsNames::d]*fields[fieldsNames::d];
  system[fieldsNames::lDO] = fields[fieldsNames::fDO]*fields[fieldsNames::fDO] - fields[fieldsNames::n] + fields[fieldsNames::m] + 2*fields[fieldsNames::d]*fields[fieldsNames::d];

std::cout<<15<<std::endl;
  return system;
}

  void BZ() const{
  std::array<T,dim> k;
  for(unsigned i=0; i<static_cast<size_t>(pow(N,dim)); ++i){
    get_k_point(i,k);
    T ek = dispersion(k);
    for(const auto& x : k) std::cout<<x<<" ";
    std::cout<<ek<<std::endl;
    if(i%N == N-1) std::cout<<std::endl;
  }
}

  void BZ(const std::vector<T>& fields){
  freeElectrons.set_fields(fields);
  std::array<T,dim> k;
  for(unsigned i=0; i<static_cast<size_t>(pow(N,dim)); ++i){
    get_k_point(i,k);
    T ek = dispersion(k);

    for(const auto& x : k) std::cout<<x<<" ";
    std::cout<<ek<<"\n";

    for(unsigned j=0; j<numberOfDistinguishableParticles; ++j){
      auto E = freeElectrons.energy(ek,fields,j);
      std::cout<<E<<" "<<Fermi_Dirac(E)<<" ";
    }

    std::cout<<std::endl;
    if(i%N == N-1) std::cout<<std::endl;
  }
}

  void BZ(const std::vector<T>& fields, const std::string& filename){
  freeElectrons.set_fields(fields);
  std::array<T,dim> k;
  std::ofstream output;
  output.open(filename.c_str());
  for(unsigned i=0; i<static_cast<size_t>(pow(N,dim)); ++i){
    get_k_point(i,k);
    T ek = dispersion(k);

    for(const auto& x : k) output<<x<<" ";
    output<<ek<<"\n";

    for(unsigned j=0; j<numberOfDistinguishableParticles; ++j){
      auto E = freeElectrons.energy(ek,fields,j);
      output<<E<<" "<<Fermi_Dirac(E)<<" ";
    }

    output<<std::endl;
    if(i%N == N-1) output<<std::endl;
  }
  output.close();
}

  std::vector<T> get_masses() const{
  return masses;
}


  T set_n   (const T&  _n){    freeElectrons.set_n(n)  ; return    n = _n; }
  T set_n   (const T&& _n){    freeElectrons.set_n(n)  ; return    n = _n; }
  T set_ef  (const T&  _ef){   freeElectrons.set_ef(ef); return   ef = _ef; }
  T set_ef  (const T&& _ef){   freeElectrons.set_ef(ef); return   ef = _ef; }
  T set_V   (const T&  _V){    freeElectrons.set_V(V)  ; return    V = _V; }
  T set_V   (const T&& _V){    freeElectrons.set_V(V)  ; return    V = _V; }
  T set_Uf  (const T&  _Uf){                             return   Uf = _Uf; }
  T set_Uf  (const T&& _Uf){                             return   Uf = _Uf; }
  T set_h   (const T&  _h){    freeElectrons.set_h(h)  ; return    h = _h; }
  T set_h   (const T&& _h){    freeElectrons.set_h(h)  ; return    h = _h; }
  T set_beta(const T&  _beta){                           return beta = _beta; }
  T set_beta(const T&& _beta){                           return beta = _beta; }

  size_t set_N(const size_t&  _N){
  N = _N;
  dk = 2*M_PI/(N-1);
  return N;
  }
  size_t set_N(const size_t&& _N){
  N = _N;
  dk = 2*M_PI/(N-1);
  return N;
  }

}; //end of class PeriodicAnderson1p1



} //end of namespace sga
#endif

