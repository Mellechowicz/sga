#ifndef SPINORBIT_H
#define SPINORBIT_H
#include <stdexcept>
#include <vector>
#include <complex>

namespace sga{

double sp_energy1(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double sp_energy2(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double sp_energy3(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double sp_energy4(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en1_d_lm(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en1_d_ln(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en1_d_qU(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en1_d_qD(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en2_d_lm(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en2_d_ln(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en2_d_qU(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en2_d_qD(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en3_d_lm(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en3_d_ln(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en3_d_qU(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en3_d_qD(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en4_d_lm(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en4_d_ln(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en4_d_qU(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);
double d_en4_d_qD(std::complex ek, std::complex mu,std::complex qU,std::complex qD,std::complex ln,std::complex lm, std::complex ef, std::complex V, std::complex h, std::complex SOC);


template<typename fieldsNames>
class SpinOrbitEnergy{
protected:
  double ef;
  double V;
  double h;
  double SOC;
  std::vector<double> fields;

public:
  SpinOrbitEnergy(const double& _V  , const double& _SOC,
                  const double& _ef , const double& _h)
                          :ef(_ef),
                           V(_V),
                           h(_h),
                           SOC(_SOC){}

  void set_ef (const double&  _ef) { ef  = _ef; }
  void set_ef (const double&& _ef) { ef  = _ef; }
  void set_V  (const double&  _V)  { V   = _V;  }
  void set_V  (const double&& _V)  { V   = _V;  }
  void set_h  (const double&  _h)  { h   = _h;  }
  void set_h  (const double&& _h)  { h   = _h;  }
  void set_SOC(const double&  _SOC){ SOC = _SOC;}
  void set_SOC(const double&& _SOC){ SOC = _SOC;}
  void set_fields(const std::vector<double>&  _fields){ fields = _fields;}
  void set_fields(const std::vector<double>&& _fields){ fields = _fields;}

  double energy(const double& _ek, const double& qfUP, const double& qfDO, size_t j){    
      if(j == 0)
        return sp_energy1(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return sp_energy2(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return sp_energy3(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return sp_energy4(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;
  }
  double d_energy_dQU(const double& _ek, const double& qfUP, const double& qfDO,
                  const std::vector<double>& fields, size_t j){
    
      if(j == 0)
        return d_en1_d_qU(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return d_en2_d_qU(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return d_en3_d_qU(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return d_en4_d_qU(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;
  }
  double d_energy_dQD(const double& _ek, const double& qfUP, const double& qfDO,
                  const std::vector<double>& fields, size_t j){
    
      if(j == 0)
        return d_en1_d_qD(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return d_en2_d_qD(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return d_en3_d_qD(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return d_en4_d_qD(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;

  }
  double d_energy_dmu(const double& _ek, const double& qfUP, const double& qfDO,
                  const std::vector<double>& fields, size_t j){
    
      if(j == 0)
        return d_en1_d_mu(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return d_en2_d_mu(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return d_en3_d_mu(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return d_en4_d_mu(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;

  }
  double d_energy_dln(const double& _ek, const double& qfUP, const double& qfDO,
                  const std::vector<double>& fields, size_t j){
    
      if(j == 0)
        return d_en1_d_ln(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return d_en2_d_ln(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return d_en3_d_ln(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return d_en4_d_ln(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;

  }
  double d_energy_dlm(const double& _ek, const double& qfUP, const double& qfDO,
                  const std::vector<double>& fields, size_t j){
    
      if(j == 0)
        return d_en1_d_lm(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 1)
        return d_en2_d_lm(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 2)
        return d_en3_d_lm(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
      else if(j == 3)
        return d_en4_d_lm(_ek,fields[fieldsNames::mu],qfUP,qfDO,fields[fieldsNames::ln],fields[fieldsNames::lm],ef,V,h,SOC);
        
    return 0.0;

  }

};

} // end of namespace sga

#endif
