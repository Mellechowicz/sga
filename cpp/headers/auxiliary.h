#ifndef HAMNONCORREL_H_
#define HAMNONCORREL_H_

#include <iostream>
#include <vector>
#include <array>
#include <functional>
#include <algorithm>

#include <cmath>

namespace sga{

template<typename T>
inline T cap(const T&& x){
  return x>T(0) ? x : T(0);
}

template<typename T>
inline T cap(const T& x){
  return x>T(0) ? x : T(0);
}

template<size_t N, typename T=double>
class DispRelation{
public:
  typedef std::array<T,N> vectorType;
  typedef std::function<T(const std::vector<T>&, const vectorType&)> Callable;
protected:
  std::vector<T> hoppings;
  Callable       dispersion;

public:
  DispRelation()=delete;
  DispRelation& operator=(const DispRelation&)=delete;

  DispRelation(const Callable&& _dispersion):dispersion(_dispersion){}

  unsigned set_hoppings(const std::vector<T>& _hoppings){
    hoppings = _hoppings;
    return hoppings.size();
  }   

  T operator()(const vectorType& momentum) const{
    return dispersion(hoppings,momentum);
  }
};

template<typename T=double>
class BandNarrowingFactors{
protected:
  T infty;
  T neighborhood;
  T flatness;

  T infty_sgn(const T& value, const T& delta, const T&& power = -0.5) const{
    return delta<flatness ? 
                           infty*value*pow(flatness, power)
                         : infty*value*pow(delta   , power);
  }
public:
  BandNarrowingFactors(T&& _infty=1e5, T&& _neighborhood=1e-4, T&& _flatness=1e-6):
    infty(_infty),neighborhood(_neighborhood),flatness(_flatness){}
/*
 *
 * NARROW UP
 *
 */
T narrow_UP(const T n,         const T m,          const T d, 
            const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;
  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn((sqrt(2.0)*d*fDO + f0*fUP)/sqrt(2.0),deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn((sqrt(2.0)*d*fDO + f0*fUP)/sqrt(2.0),deltaB);
  }

  if(d <= T(0)){
    return (f0*fUP)/sqrt(deltaA*deltaB);
  }
#endif

  return (sqrt(2.0)*d*fDO + f0*fUP)/sqrt(deltaA*deltaB);
}


/*
 *
 * NARROW UP
 * Dn
 *
 */
T narrow_UP_dn(const T n,         const T m,          const T d, 
               const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;
  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn((sqrt(2.0)*d*fDO + f0*fUP)/(2.*sqrt(2.0)) ,deltaA,-1.5);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(-(sqrt(2.0)*d*fDO + f0*fUP)/(2.*sqrt(2.0)) ,deltaB,-1.5);
  }

  if(d <= T(0)){
    return (f0*fUP*(-1 + m + n))/pow(deltaA*deltaB,1.5);
  }
#endif
  return ((sqrt(2.0)*d*fDO + f0*fUP)*(-1 + m + n))/pow(deltaA*deltaB,1.5) ;
}

/*
 *
 * NARROW UP
 * Dm
 *
 */
T narrow_UP_dm(const T n,         const T m,          const T d, 
               const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;
  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn((sqrt(2.0)*d*fDO + f0*fUP)/(2.*sqrt(2.0)) ,deltaA, -1.5);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(-(sqrt(2.0)*d*fDO + f0*fUP)/(2.*sqrt(2.0)) ,deltaB, -1.5);
  }

  if(d <= T(0)){
    return (f0*fUP*(-1 + m + n))/pow(deltaA*deltaB,1.5);
  }
#endif
  return ((sqrt(2.0)*d*fDO + f0*fUP)*(-1 + m + n))/pow(deltaA*deltaB,1.5) ;
}

/*
 *
 * NARROW UP
 * Dd
 *
 */
T narrow_UP_dd(const T n,         const T m,          const T d, 
               const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;

  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(fDO ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(fDO,deltaB);
  }

  if(d <= T(0)){
    return (sqrt(2.0)*fDO)/sqrt(deltaA*deltaB);
  }
#endif
  return (sqrt(2.0)*fDO)/sqrt(deltaA*deltaB);
}

/*
 *
 * NARROW UP
 * Df0
 *
 */
T narrow_UP_df0(const T n,         const T m,          const T d, 
                const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;

  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(fUP/(sqrt(2.0)) ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(fUP/(sqrt(2.0)) ,deltaB);
  }
#endif
  return fUP/sqrt(deltaA*deltaB) ;
}

/*
 *
 * NARROW UP
 * DfUP
 *
 */
T narrow_UP_dfUP(const T n,         const T m,          const T d, 
                 const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;

  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(f0/(sqrt(2.0)) ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(f0/(sqrt(2.0)),deltaB);
  }
#endif
  return f0/sqrt(deltaA*deltaB) ;
}

/*
 *
 * NARROW UP
 * DfDO
 *
 */
T narrow_UP_dfDO(const T n,         const T m,          const T d, 
                 const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T deltaA = 2-n-m;
  T deltaB = n+m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(d,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(d,deltaB);
  }

  if(d <= 0)
    return 0. ;
#endif
  return (sqrt(2.0)*d)/sqrt(deltaA*deltaB);
}

/*
 *
 *
 * NARROW DO
 *
 *
 *
 */
T narrow_DO(const T n,         const T m,          const T d, 
            const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn((sqrt(2.0)*d*fUP + f0*fDO)/sqrt(2.0),deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn((sqrt(2.0)*d*fUP + f0*fDO)/sqrt(2.0),deltaB);
  }

  if(d <= T(0)){
    return (f0*fDO)/sqrt(deltaA*deltaB);
  }
#endif

  return (sqrt(2.0)*d*fUP + f0*fDO)/sqrt(deltaA*deltaB);
}

/*
 *
 * NARROW DO
 * Dn
 *
 */
 T narrow_DO_dn(const T n,         const T m,          const T d, 
                const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn((f0*fDO + sqrt(2.0)*d*fUP)/(2.*sqrt(2.0)),deltaA,-1.5);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(-(f0*fDO + sqrt(2.0)*d*fUP)/(2.*sqrt(2.0)),deltaB,-1.5);
  }

  if(d <= T(0)){
    return -((f0*fDO*(1 + m - n))/pow(deltaA*deltaB,1.5));
  }
#endif
  return -(((f0*fDO + sqrt(2.0)*d*fUP)*(1 + m - n))/pow(deltaA*deltaB,1.5)) ;

}

/*
 *
 * NARROW DO
 * Dm
 *
 */
 T narrow_DO_dm(const T n,         const T m,          const T d, 
                const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(-(f0*fDO + sqrt(2.0)*d*fUP)/(2.*sqrt(2.0)),deltaB,-1.5);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn((f0*fDO + sqrt(2.0)*d*fUP)/(2.*sqrt(2.0)),deltaB,-1.5);
  }

  if(d <= T(0)){
    return (f0*fDO*(1 + m - n))/pow(deltaA*deltaB,1.5);
  }
#endif
  return ((f0*fDO + sqrt(2.0)*d*fUP)*(1 + m - n))/pow(deltaA*deltaB,1.5) ;

}

/*
 *
 * NARROW DO
 * Dd
 *
 */
 T narrow_DO_dd(const T n,         const T m,          const T d, 
                const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T fUP = _fUP< T(0) ? sqrt(cap(n-m-2*d*d))   : _fUP;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(fUP ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(fUP ,deltaB);
  }

  if(d <= T(0)){
    return (sqrt(2.0)*fUP)/sqrt(deltaA*deltaB);
  }
#endif
  return (sqrt(2.0)*fUP)/sqrt(deltaA*deltaB);

}

/*
 *
 * NARROW DO
 * Df0
 *
 */
 T narrow_DO_df0(const T n,         const T m,          const T d, 
                 const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T fDO = _fDO< T(0) ? sqrt(cap(n+m-2*d*d))   : _fDO;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(fDO/sqrt(2.0) ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(fDO/sqrt(2.0) ,deltaB);
  }
#endif
  return fDO/sqrt(deltaA*deltaB) ;

}

/*
 *
 * NARROW DO
 * DfUP
 *
 */
 T narrow_DO_dfUP(const T n,         const T m,          const T d, 
                  const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(d ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(d ,deltaB);
  }

  if(d <= T(0)){
    return 0.;
  }
#endif
  return (sqrt(2.0)*d)/sqrt(deltaA*deltaB) ;

}

/*
 *
 * NARROW DO
 * DfDO
 *
 */
 T narrow_DO_dfDO(const T n,         const T m,          const T d, 
                  const T _f0 __attribute__((unused)) = T(-1),
                  const T _fUP __attribute__((unused)) = T(-1),
                  const T _fDO __attribute__((unused)) = T(-1)
                 ) const{
  T f0  = _f0 < T(0) ? sqrt(cap(2-2*n+2*d*d)) : _f0;

  T deltaA = 2-n+m;
  T deltaB = n-m;

#ifndef _UNSAFE
  if(deltaA*deltaB <= T(0)){
    return T(0);
  }

  if(n >= T(1)){
    if(deltaA < neighborhood) 
      return infty_sgn(f0/sqrt(2.0) ,deltaA);
  } else{
    if(deltaB < neighborhood)
      return infty_sgn(f0/sqrt(2.0) ,deltaB);
  }
#endif
  return f0/sqrt(deltaA*deltaB);

}

};

} //end namespace sga

#endif
