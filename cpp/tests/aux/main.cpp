#include <iostream>
#include <vector>
#include "../../headers/auxiliary.h"

int main(){
  sga::DispRelation<3> ek([](const std::vector<double>& t, const std::array<double,3>& k){return k[0];});
  std::cout<<ek({1.0,0.0,0.0})<<std::endl;

  sga::BandNarrowingFactors<double> q;

  for(double d = 0.0; d < 0.25; d+=0.01) std::cout<<(q.narrow_UP(0.99,0.0,d))<<std::endl;

  return 0;
}
