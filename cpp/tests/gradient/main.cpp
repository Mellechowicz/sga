#include <iostream>
#include <algorithm>
#include <tuple>

#include "../../headers/hamiltonian.h"

#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>


template<class Hamiltonian, size_t N>
int landau_derivatives(const gsl_vector *input,
                                  void *params,
                             gsl_vector *output){
  std::vector<double> fields(N,0.0);
  auto H = static_cast<Hamiltonian*>(params);

  for(size_t i = 0; i<N; ++i)
    fields[i] = gsl_vector_get(input, i);

  auto eqs = H->system_of_equations(fields);

  if(N != eqs.size())
    return GSL_FAILURE;

  for(size_t i = 0; i<N; ++i)
    gsl_vector_set(output, i, eqs[i]);

  return GSL_SUCCESS;
}

template<class Hamiltonian, size_t N>
int print_state (size_t iter, gsl_multiroot_fsolver * s, Hamiltonian* H = nullptr){
  std::vector<double> fields(N);
  for(size_t i=0; i<N; ++i)
    fields[i] = gsl_vector_get (s->x, i);

  double E = 1e199;
  if( H != nullptr)
    E = H->free_energy(fields);

  std::cout<<E<<" ";

  auto ms = H->get_masses();
  std::cout<<ms[0]<<" "<<ms[1]<<" ";

  std::cout<<"("<<iter<<")\t";
  for(size_t i=0; i<N; ++i)
    std::cout<<gsl_vector_get (s->x, i)<<" ";
  std::cout<<"\t";

  double error = 0.0;
  for(size_t i=0; i<N; ++i)
    error += fabs(gsl_vector_get(s->f, i));
  std::cout<<error<<" ";

  std::cout<<"\t";
  double field_error = 0.0;
  field_error += gsl_vector_get(s->x,3)*gsl_vector_get(s->x,3) - 2. + 2*gsl_vector_get(s->x,0) - 2*gsl_vector_get(s->x,2)*gsl_vector_get(s->x,2);
  field_error += gsl_vector_get(s->x,4)*gsl_vector_get(s->x,4) - gsl_vector_get(s->x,0) - gsl_vector_get(s->x,1) + 2*gsl_vector_get(s->x,2)*gsl_vector_get(s->x,2);
  field_error += gsl_vector_get(s->x,5)*gsl_vector_get(s->x,5) - gsl_vector_get(s->x,0) + gsl_vector_get(s->x,1) + 2*gsl_vector_get(s->x,2)*gsl_vector_get(s->x,2);
  std::cout<<field_error<<std::endl;

  return GSL_SUCCESS;
}


int main(int argc, char** argv){
  if(argc < 4){
    std::cerr<<"Feed me parameters: n V U"<<std::endl;
    return -1;
  }

  const size_t N = 12;
  sga::PeriodicAnderson1p1 H;
  H.set_n (std::stod(argv[1]));
  H.set_V (std::stod(argv[2]));
  H.set_Uf(std::stod(argv[3]));

  std::cout<<"# n = "<<argv[0]<<"V = "<<argv[1]<<"Uf= "<<argv[2]<<std::endl<<"#"<<std::endl<<"#"<<std::endl;

  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;
  T = gsl_multiroot_fsolver_hybrids;
  s = gsl_multiroot_fsolver_alloc (T,N);

  int status;
  size_t iter = 0;

  gsl_multiroot_function f = {&landau_derivatives<sga::PeriodicAnderson1p1,N>, N, static_cast<void*>(&H)};

  gsl_vector *x = gsl_vector_alloc(N);

  std::vector<std::tuple<double,double,double>> phases;
  phases.push_back(std::make_tuple(0.9*std::stod(argv[1]),0.0,0.0));
  phases.push_back(std::make_tuple(0.9*std::stod(argv[1]),0.0,0.4));
  phases.push_back(std::make_tuple(0.9*std::stod(argv[1]),0.4*std::stod(argv[1]),0.0));
  phases.push_back(std::make_tuple(0.9*std::stod(argv[1]),0.4*std::stod(argv[1]),0.4));
  std::vector<double> betas({ 8.01 , 9.51366 , 11.3137 , 13.4543 , 16.01 , 19.0273 , 22.6274 , 26.9087 , 32.01 , 38.0546 , 45.2548 , 53.8174 , 64.01 , 76.1093 , 90.5097 , 107.635 , 128.01 , 152.219 , 181.019 , 215.269 , 256.01 , 304.437 , 362.039 , 430.539 , 512.01 , 608.874 , 724.077 , 861.078 , 1024.01 , 1217.75 , 1448.15 , 1722.16 , 2048.01, 2435.5 , 2896.31 , 3444.31 , 4096.01});

  std::vector<size_t> sizes({64,128,256,512,1024,2048});

  size_t count = 0;
  for(const auto& ph : phases){
    std::cout<<"# PHASE: "<<count++<<std::endl<<"#"<<std::endl;
    std::cout<<"# size beta energy massUp massDo #iter nf mf df f0 fu fd lnf lmf mu lf0 lfu lfd sum_vals field_err"<<std::endl;
    auto n = std::get<0>(ph);
    auto m = std::get<1>(ph);
    auto d = std::get<2>(ph);

    for(const size_t& _size : sizes){
      H.set_N(_size);
      auto fields = std::vector<double>({n,m,d,sqrt(fabs(2-2*n+2*d*d)),sqrt(fabs(n-m-2*d*d)),sqrt(fabs(n+m-2*d*d)),-0.1,-0.1,-2.0,1.0,1.0,1.0});     
      for(const double& _beta : betas){
        H.set_beta(_beta);

        for(size_t i=0; i<N; ++i)
          gsl_vector_set (x, i, fields[i]);
        gsl_multiroot_fsolver_set (s, &f, x);
      
        iter = 0;
        bool IS_STUCK = false;
        do{
            iter++;
            status = gsl_multiroot_fsolver_iterate (s);
      
            if (status)   /* check if solver is stuck */
              IS_STUCK = true;
            else
              status = gsl_multiroot_test_residual (s->f, 1e-13);

	    if(status != GSL_SUCCESS && status != GSL_CONTINUE && !IS_STUCK){
              n *= 1.01;
	      m *= 0.9;
              d *= 0.9;
              fields = std::vector<double>({n,m,d,sqrt(fabs(2-2*n+2*d*d)),sqrt(fabs(n-m-2*d*d)),sqrt(fabs(n+m-2*d*d)),-0.1,-0.1,-2.0,1.0,1.0,1.0});     

              for(size_t i=0; i<N; ++i)
                gsl_vector_set (x, i, fields[i]);
              gsl_multiroot_fsolver_set (s, &f, x);
            }
          } while (status == GSL_CONTINUE && iter < 1000);
      
        if(status == GSL_SUCCESS){
          std::cout<<_size<<" "<<_beta<<"\t";
          print_state<sga::PeriodicAnderson1p1,N>(iter, s, &H);
          for(size_t i=0; i<N; ++i)
            fields[i] = gsl_vector_get (s->x, i);
          std::string fileName = "output/BZone_";
          fileName += std::to_string(static_cast<int>(_size));
          fileName += ".dat"; 
	  if(_beta > 2519.0) H.BZ(fields,fileName);
        }
      }
    }
  }

  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);
  return 0;
}

