#include <iostream>
#include <complex>

#include "../../headers/spinorbit.h"

int main(){
  enum Enum{n,m,d,f0,fUP,fDO,ln,lm,mu,l0,lUP,lDO};

  double ef = -3.0;
  double V = -0.9;
  double h = 0.0;
  double SOC = 0.01;

  sga::SpinOrbitEnergy<std::complex<double>,double,Enum> SOE(ef,V,h,SOC);

  double ek = -4.0;
  for(double mu = 0.0; mu > -2.0; mu-=0.001){
  std::cout<<mu<<" ";
  std::cout<<SOE.sp_energy1(ek,mu,0.8,0.8,-0.1,-0.1)<<" ";
  std::cout<<SOE.sp_energy2(ek,mu,0.8,0.8,-0.1,-0.1)<<" ";
  std::cout<<SOE.sp_energy3(ek,mu,0.8,0.8,-0.1,-0.1)<<" ";
  std::cout<<SOE.sp_energy4(ek,mu,0.8,0.8,-0.1,-0.1)<<std::endl;
  }

  return 0;
}
