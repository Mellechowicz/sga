#include <iostream>
#include <complex>

#include "../../headers/auxiliary.h"
#include "../../headers/hamtemplate.h"
#include "../../headers/spinorbit.h"

int main(){

  sga::AndersonHamiltonian<double, sga::SpinOrbitEnergy<sga::StandardEnum>,
                     sga::BandNarrowingFactors<double>,
                     sga::DispRelation<2,double>,sga::StandardEnum,4,2> H(1.5,-3.0,-0.9,8.0,256,256,0.0,0.01,
sga::DispRelation<2,double>([](const std::vector<double>& t, const std::array<double,2>& k){
  return t[0]*(std::cos(k[0]) + std::cos(k[1])) + 4*t[1]*(std::cos(k[0])*std::cos(k[1]));
}));

  H.dispersion.set_hoppings(std::vector<double>({-1.0,0.25}));

  auto n = 1.0;
  auto m = 0.0;
  auto d = 0.4;
  std::vector<double> fields({n,m,d,sqrt(fabs(2-2*n+2*d*d)),sqrt(fabs(n-m-2*d*d)),sqrt(fabs(n+m-2*d*d)),-0.1,-0.1,-2.0,1.0,1.0,1.0});    

  for(double mu = 0.0; mu > -2.0; mu-=0.001){
    std::cout<<mu<<" ";
    fields[sga::StandardEnum::mu] = mu;
    std::cout<<H.free_energy(fields);
    std::cout<<std::endl;
  }

  return 0;
}
