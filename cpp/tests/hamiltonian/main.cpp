#include <iostream>
#include <algorithm>
#include "../../headers/hamiltonian.h"

#include <gsl/gsl_multimin.h>
#include <gsl/gsl_siman.h>

template<class Hamiltonian, size_t N>
double landau_energy(const gsl_vector *v, void *params){
  std::vector<double> fields(N,0.0);
  auto H = static_cast<Hamiltonian*>(params);

  for(size_t i = 0; i<N; ++i)
    fields[i] = gsl_vector_get(v, i);

  return H->Landau(fields);
}

template<class Data, size_t N>
double landau_energy_ASA(void *xp){
  auto x = static_cast<Data*>(xp)->vector;
  auto H = static_cast<Data*>(xp)->hamiltonian;

  auto E = H->Landau(*x);

  return E;
}

template<class Data, size_t N>
double measure(void *xp, void *yp) {
  auto x = static_cast<Data*>(xp)->vector;
  auto y = static_cast<Data*>(yp)->vector;

  double m = 0.0;
  for(unsigned i=0; i<N; ++i)
    m += pow((*x)[i] - (*y)[i],2);

  return sqrt(m);
}

template<class Data, size_t N>
void step(const gsl_rng* r, void *xp, double step_size) {
  auto x = static_cast<Data*>(xp)->vector;
  double u;

  do{
    u = gsl_rng_uniform(r);
    (*x)[0] += u * 2 * step_size - step_size;
  }while((*x)[0] < 0.0 || (*x)[0] > 2.0);
  do{
    u = gsl_rng_uniform(r);
    (*x)[1] += u * 2 * step_size - step_size;
  }while(fabs((*x)[1]) > (*x)[0] || (*x)[1] < 0.0);
  do{
    u = gsl_rng_uniform(r);
    (*x)[2] += u * 2 * step_size - step_size;
  }while((*x)[2] < 0.0 || (*x)[2] > sqrt(0.25*((*x)[0]-fabs((*x)[1]))));

  for(unsigned i=3; i<6; ++i){
    u = gsl_rng_uniform(r);
    (*x)[i] += u * 2 * step_size * step_size - step_size * step_size;
  }

  for(unsigned i=6; i<N; ++i){
    u = gsl_rng_uniform(r);
    (*x)[i] += u * 2 * step_size - step_size;
  }
}

template<class Data, size_t N>
void print(void *xp) {
  auto x = static_cast<Data*>(xp)->vector;
  for(unsigned i=0; i<N; ++i)
    std::cout<<" "<<(*x)[i]<<" ";
}

int main(){
  sga::PeriodicAnderson1p1 H;
  auto n = 1.501231241;
  auto m = 0.24512451243;
  auto d = 0.0;
  auto fields = std::vector<double>({n,m,d,sqrt(fabs(2-2*n+2*d*d)),sqrt(fabs(n-m-2*d*d)),sqrt(fabs(n+m-2*d*d)),-0.1,-0.1,-3.0});     

  struct Data{
    std::vector<double>* vector;
    sga::PeriodicAnderson1p1* hamiltonian;
  } init;

  init.vector = &fields;
  init.hamiltonian = &H;

  gsl_siman_params_t params = {200,          10000,        1e-2,  1.0,  0.01,     1.8, 0.001};
                            //{N_TRIES, ITERS_FIXED_T, STEP_SIZE, K, T_INITIAL, MU_T, T_MIN};
  const gsl_rng_type * T;
  gsl_rng * r;

  gsl_rng_env_setup();

  T = gsl_rng_default;
  r = gsl_rng_alloc(T);

  const gsl_multimin_fminimizer_type *minimizer =
    gsl_multimin_fminimizer_nmsimplex2;
  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *ss, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  /* Starting point */
  x = gsl_vector_alloc(9);
  ss = gsl_vector_alloc (9);
  s = gsl_multimin_fminimizer_alloc(minimizer, 9);

  /* Set initial step sizes to 1 */
  gsl_vector_set_all (ss, 1e-4);
  gsl_vector_set (ss, 2, 1e-6);

  /* Initialize method and iterate */
  minex_func.n = 9;
  minex_func.f = landau_energy<sga::PeriodicAnderson1p1,9>;
  minex_func.params = static_cast<void*>(&H);

 
  gsl_siman_solve(r, &init, landau_energy_ASA<Data,9>, step<Data,9>, measure<Data,9>, print<Data,9>,
                  NULL, NULL, NULL,
                  sizeof(init), params);
for(double h = 0.0; h<0.1; h+=0.01){
  params = {200,          100,        1e-3,  1.0,  0.0001,     2, 2e-10};
  H.set_h(h);
  gsl_siman_solve(r, &init, landau_energy_ASA<Data,9>, step<Data,9>, measure<Data,9>, print<Data,9>,
                  NULL, NULL, NULL,
                  sizeof(init), params);

  std::cerr<<h<<"\t";
  for(unsigned i=0; i<9; ++i)
    std::cerr<<" "<<fields[i]<<" ";
  std::cerr<<H.free_energy(fields)<<std::endl;
  auto m = H.get_masses();
  std::cerr<<m[0]<<" "<<m[1]<<std::endl;


// NSIMPLEX
//
  for(size_t i = 0; i<9; ++i)
    gsl_vector_set(x, i, fields[i]);
  gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
  iter = 0;
  do {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status)
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-5);

      if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

      std::cout<<"("<<iter<<")\t";
      for(size_t i = 0; i<9; ++i)
        std::cout<<gsl_vector_get(s->x, i)<<" ";
      printf("f() = %7.3f size = %.7f\n", s->fval, size);

      if(abs(gsl_vector_get(s->x, 0)) > 2){
	std::cerr<<" nf  > 2 !!!"<<std::endl;
        gsl_vector_set(x, 0, 0.5*gsl_vector_get(s->x, 0));
        gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
      }
	
      if(abs(gsl_vector_get(s->x, 1)) > gsl_vector_get(s->x, 0)){
	std::cerr<<"|mf| > nf !!!"<<std::endl;
        gsl_vector_set(x, 1, 0.5*gsl_vector_get(s->x, 1));
        gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
      }
	
      if(gsl_vector_get(s->x, 2) < 0){
	std::cerr<<"  d  <  0 !!!"<<std::endl;
        gsl_vector_set(x, 2, -2*gsl_vector_get(s->x, 2));
        gsl_multimin_fminimizer_set (s, &minex_func, x, ss);
      }
	
    }
  while (status == GSL_CONTINUE && iter < 100000);

  for(size_t i = 0; i<9; ++i)
    fields[i] = gsl_vector_get(s->x, i);

  std::cerr<<h<<"\t";
  for(unsigned i=0; i<9; ++i)
    std::cerr<<" "<<fields[i]<<" ";
  std::cerr<<H.free_energy(fields);
  m = H.get_masses();
  std::cerr<<m[0]<<" "<<m[1]<<std::endl;

}

  gsl_rng_free (r);
  gsl_vector_free(x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);

  return 0;
}

