#include "../headers/hamiltonian.h"

namespace sga{
PeriodicAnderson1p1::PeriodicAnderson1p1():dispersion(
         [](const std::vector<double>& t, const std::array<double,2>& k){
             return 2*t[0]*(cos(k[0])+cos(k[1]));
  }){
  dispersion.set_hoppings({-1.0});
  n  = 1.5;
  ef = -3.0;
  V  = -1.5;
  Uf = 10.0;
  h  = 0.0;

  beta = 20.0;

  N  = 100;
  dk = 2*M_PI/(N-1);

  masses = std::vector<double>(2,0.0);
}

double PeriodicAnderson1p1::singe_particle_energy(const double ek, const std::vector<double>& fields, int spin, int eigenSplitter){
  if(spin>0){
    auto qf = qs.narrow_UP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
    if (qf > 1e-12) masses[0] = 1.0/qf;
    else            masses[0] = 1e12;

    return .5*(ef + ek 
           - 2*fields[8] - fields[6]
           - 2*h - fields[7] 
           - eigenSplitter*sqrt(pow( 
           (ef - fields[6] - fields[7]) - ek,2)  + 4*qf*qf*V*V));
  } else {
    auto qf = qs.narrow_DO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
    if (qf > 1e-12) masses[1] = 1.0/qf;
    else            masses[1] = 1e12;

    return .5*(ef + ek 
           - 2*fields[8] -  fields[6]
           + 2*h + fields[7] 
           - eigenSplitter*sqrt(pow( 
          (ef - fields[6] + fields[7]) - ek,2)  + 4*qf*qf*V*V));
  }
}
  
double PeriodicAnderson1p1::log_exp(double E)const{
  if(E<0)
    return E-log(1.0 + exp(beta*E))/beta; 
  else
    return -log(1.0 + exp(-beta*E))/beta; 
}

double PeriodicAnderson1p1::Fermi_Dirac(double E) const{ 
//  std::cout<<E<<" "<<beta<<" "<<beta*E<<" "<<tanh(0.5*beta*E)<<std::endl;
  return 0.5*(1.0 - tanh(0.5*beta*E));
}

double PeriodicAnderson1p1::Landau(const std::vector<double>& fields){
  auto E = free_energy(fields);
  E -= fields[8]*n;

  return E;
}

double PeriodicAnderson1p1::free_energy(const std::vector<double>& fields){
  double E  = 0.0;
  #pragma omp parallel for reduction (+:E)
  for(unsigned i=0; i<N*N; ++i){
    double ek = dispersion({(i/N)*dk - M_PI,(i%N)*dk - M_PI});
    E += log_exp(singe_particle_energy(ek,fields, 1, 1));
    E += log_exp(singe_particle_energy(ek,fields,-1, 1));
    E += log_exp(singe_particle_energy(ek,fields, 1,-1));
    E += log_exp(singe_particle_energy(ek,fields,-1,-1));
  }

  E /= N*N;

  E += fields[6]*fields[0] + fields[7]*fields[1];
  E += Uf*fields[2]*fields[2];
  E += fields[9]*(fields[3]*fields[3] - 2. + 2*fields[0] - 2*fields[2]*fields[2]);
  E += fields[10]*(fields[4]*fields[4] - fields[0] - fields[1] + 2*fields[2]*fields[2]);
  E += fields[11]*(fields[5]*fields[5] - fields[0] + fields[1] + 2*fields[2]*fields[2]);

  E += fields[8]*n;

  return E;
}

std::vector<double> PeriodicAnderson1p1::system_of_equations(const std::vector<double>& fields){
  std::vector<double> system(12,0.0);
  double Xn  = 0.0;
  double Xuf = 0.0;
  double Xdf = 0.0;
  double Up  = 0.0;
  double Do  = 0.0;
  auto qfUP  = qs.narrow_UP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  auto qfDO  = qs.narrow_DO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  #pragma omp parallel for reduction (+:Xn,Xuf,Xdf,Up,Do)
  for(unsigned i=0; i<N*N; ++i){
    double kx = (i/N)*dk - M_PI;
    double ky = (i%N)*dk - M_PI;
    double ek = dispersion({kx,ky});
    auto EpU = Fermi_Dirac(singe_particle_energy(ek,fields, 1, 1));
    auto EpD = Fermi_Dirac(singe_particle_energy(ek,fields,-1, 1));
    auto EmU = Fermi_Dirac(singe_particle_energy(ek,fields, 1,-1));
    auto EmD = Fermi_Dirac(singe_particle_energy(ek,fields,-1,-1));

    auto Lu = (ef - ek - fields[6] - fields[7]);
    auto Mu = 1.0/(sqrt(pow(Lu,2)  + 4*qfUP*qfUP*V*V));
    Lu *= Mu;

    auto Ld = (ef - ek - fields[6] + fields[7]);
    auto Md = 1./(sqrt(pow(Ld,2)  + 4*qfDO*qfDO*V*V));
    Ld *= Md;

    Up += EpU*(Lu-1.)-EmU*(Lu+1.);
    Do += EpD*(Ld-1.)-EmD*(Ld+1.);

    Xn  += EpU+EpD+EmU+EmD;
    Xuf += (EpU-EmU)*Mu;
    Xdf += (EpD-EmD)*Md;
  }

  Xn  /= N*N;
  Xuf /= N*N;
  Xdf /= N*N;
  Xuf *= -2.*qfUP*V*V;
  Xdf *= -2.*qfDO*V*V;
  double Xlnf = 0.5*(Up + Do)/(N*N);
  double Xlmf = 0.5*(Up - Do)/(N*N);

  system[0] = fields[6] + 2*fields[9] - fields[10] - fields[11] 
             + Xuf*qs.narrow_UP_dn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) 
             + Xdf*qs.narrow_DO_dn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[1] = fields[7] - fields[10] + fields[11] 
             + Xuf*qs.narrow_UP_dm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
             + Xdf*qs.narrow_DO_dm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[2] = 2*fields[2]*(Uf + 2*(-fields[9] + fields[10] + fields[11]))
             + Xuf*qs.narrow_UP_dd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
             + Xdf*qs.narrow_DO_dd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[3] = 2*fields[3]*fields[9]
             + Xuf*qs.narrow_UP_df0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
             + Xdf*qs.narrow_DO_df0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[4] = 2*fields[4]*fields[10]
             + Xuf*qs.narrow_UP_dfUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
             + Xdf*qs.narrow_DO_dfUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[5] = 2*fields[5]*fields[11]
             + Xuf*qs.narrow_UP_dfDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
             + Xdf*qs.narrow_DO_dfDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]);
  system[6] = fields[3]*fields[3] - 2. + 2*fields[0] - 2*fields[2]*fields[2];
  system[7] = fields[4]*fields[4] - fields[0] - fields[1] + 2*fields[2]*fields[2];
  system[8] = fields[5]*fields[5] - fields[0] + fields[1] + 2*fields[2]*fields[2];
  system[9] = fields[0] + Xlnf;
  system[10]= fields[1] + Xlmf;
  system[11]= n - Xn;

  return system;
}

void PeriodicAnderson1p1::BZ() const{
  for(unsigned i=0; i<N*N; ++i){
    std::cout<<(i/N)*dk - M_PI<<" "<<(i%N)*dk - M_PI<<" "<<dispersion({(i/N)*dk - M_PI,(i%N)*dk - M_PI})<<std::endl;
    if(i%N == N-1) std::cout<<std::endl;
  }
}

void PeriodicAnderson1p1::BZ(const std::vector<double>& fields){
  for(unsigned i=0; i<N*N; ++i){
    auto k0 = (i/N)*dk - M_PI;
    auto k1 = (i%N)*dk - M_PI;
    auto ek = dispersion({k0,k1});
    auto EpU = singe_particle_energy(ek,fields, 1, 1);
    auto EpD = singe_particle_energy(ek,fields,-1, 1);
    auto EmU = singe_particle_energy(ek,fields, 1,-1);
    auto EmD = singe_particle_energy(ek,fields,-1,-1);

    std::cout<<k0<<" "<<k1<<" "<<ek<<" "<<EpU<<" "<<Fermi_Dirac(EpU)<<" "<<EpD<<" "<<Fermi_Dirac(EpD)<<" "<<EmU<<" "<<Fermi_Dirac(EmU)<<" "<<EmD<<" "<<Fermi_Dirac(EmD)<<std::endl;

    if(i%N == N-1) std::cout<<std::endl;
  }
}

void PeriodicAnderson1p1::BZ(const std::vector<double>& fields, const std::string& filename){
  std::ofstream output;
  output.open(filename.c_str());
  for(unsigned i=0; i<N*N; ++i){
    auto k0 = (i/N)*dk - M_PI;
    auto k1 = (i%N)*dk - M_PI;
    auto ek = dispersion({k0,k1});
    auto EpU = singe_particle_energy(ek,fields, 1, 1);
    auto EpD = singe_particle_energy(ek,fields,-1, 1);
    auto EmU = singe_particle_energy(ek,fields, 1,-1);
    auto EmD = singe_particle_energy(ek,fields,-1,-1);

    output<<k0<<" "<<k1<<" "<<ek<<" "<<EpU<<" "<<Fermi_Dirac(EpU)<<" "<<EpD<<" "<<Fermi_Dirac(EpD)<<" "<<EmU<<" "<<Fermi_Dirac(EmU)<<" "<<EmD<<" "<<Fermi_Dirac(EmD)<<std::endl;

    if(i%N == N-1) output<<std::endl;
  }
  output.close();
}

std::vector<double> PeriodicAnderson1p1::get_masses() const{
  return masses;
}

double PeriodicAnderson1p1::set_n   (const double&  _n){
  return n = _n;
}
double PeriodicAnderson1p1::set_n   (const double&& _n){
  return n = _n;
}
double PeriodicAnderson1p1::set_ef  (const double&  _ef){
  return ef = _ef;
}
double PeriodicAnderson1p1::set_ef  (const double&& _ef){
  return ef = _ef;
}
double PeriodicAnderson1p1::set_V   (const double&  _V){
  return V = _V;
}
double PeriodicAnderson1p1::set_V   (const double&& _V){
  return V = _V;
}
double PeriodicAnderson1p1::set_Uf  (const double&  _Uf){
  return Uf = _Uf;
}
double PeriodicAnderson1p1::set_Uf  (const double&& _Uf){
  return Uf = _Uf;
}
double PeriodicAnderson1p1::set_h   (const double&  _h){
  return h = _h;
}
double PeriodicAnderson1p1::set_h   (const double&& _h){
  return h = _h;
}
double PeriodicAnderson1p1::set_beta(const double&  _beta){
  return beta = _beta;
}
double PeriodicAnderson1p1::set_beta(const double&& _beta){
  return beta = _beta;
}
size_t PeriodicAnderson1p1::set_N(const size_t&  _N){
  N = _N;
  dk = 2*M_PI/(N-1);
  return N;
}
size_t PeriodicAnderson1p1::set_N(const size_t&& _N){
  N = _N;
  dk = 2*M_PI/(N-1);
  return N;
}

} //end of namespace sga
