#!/usr/bin/python

import PAM.hamiltonian as ham
import numpy as np

Lambda=1e2
kxMax = np.pi
kyMax = np.pi
kxs = np.linspace(-kxMax,kxMax,Lambda)
kys = np.linspace(-kyMax,kyMax,Lambda)
kxxs,kyys = np.meshgrid(kxs,kys)

ks = np.stack((np.ravel(kxxs), np.ravel(kyys)), axis=-1)

def eps(ts,ks): # SC lattice with only NN hopping
  return 2*ts[0]*(np.cos(ks[:,0])+np.cos(ks[:,1]))

epsSC = ham.DispRel(eps=eps,BZone=ks)
epsSC(out='data.dat')
