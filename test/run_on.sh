#!/bin/bash

end=$1

if [ -z $1 ]; then
  end="1"
fi

for i in $(seq $end); do
  echo $i
  ./run.sh "${i}" & echo "Run"
done
