#!/bin/bash

u=$1

if [ -z $1 ]; then
  u=1
fi

U=$(awk -vU=$u 'BEGIN{print 3.5 + 0.5*U}')
for i in $(seq -5 1 5); do
  h=$(awk -vI=$i 'BEGIN{print 0.01*I}')
  for j in $(seq 7); do
    V=$(awk -vJ=$j 'BEGIN{print -0.3 - 0.1*J}')
    ./FC.py 512 1.5 $h $V $U > res_h${h}_V${V}_U${U}.dat 2> err_h${h}_V${V}_U${U}.dat 
  done
done
