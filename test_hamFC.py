#!/usr/local/bin/python

import PAM.hamNonCorrel as ham
import numpy as np
from scipy.optimize import root

Lambda = 512 
kxMax = np.pi
kyMax = np.pi
kxs = np.linspace(-kxMax,kxMax,Lambda)
kys = np.linspace(-kyMax,kyMax,Lambda)
kxxs,kyys = np.meshgrid(kxs,kys)

ks = np.stack((np.ravel(kxxs), np.ravel(kyys)), axis=-1)

def eps(ts,ks): # SC lattice with only NN hopping
  return 2*ts[0]*(np.cos(ks[:,0])+np.cos(ks[:,1]))

n  = 1.7
ef = -3.
V  = -0.7
h  = 0.
Uf = 5.
beta = 100.

epsSC = ham.DispRel(eps=eps,BZone=ks,t=np.array([-1.0]))
PAM = ham.FreeElectron(params=[n,ef,V,Uf,h],beta=beta,eps=epsSC)

nf0 = np.abs(2.-n)
mf0 = 0.
df0 = .02
f0f0 = np.sqrt(np.abs(2-2*nf0+2*df0))
fuf0 = np.sqrt(np.abs(nf0-mf0-2*df0))
fdf0 = np.sqrt(np.abs(nf0+mf0-2*df0))

mets = ['hybr','lm','broyden1','broyden2','anderson','linearmixing','diagbroyden','excitingmixing','krylov','df-sane']
opts = [{'maxfev':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxiter':10000},{'maxfev':10000}]

#met = mets[1]

for met,opt in zip(mets,opts):
  l1 = (63-len(met))/2
  l2 = (64-len(met))/2
  
  print "*****************************************************************************************"
  print "*****************************************************************************************"
  print "*****************************************************************************************"
  print "*************%s%s%s*************"%(l1*" ",met,l2*" ")
  print "*************%s%s%s*************"%(l1*" ",met,l2*" ")
  print "*************%s%s%s*************"%(l1*" ",met,l2*" ")
  print "*****************************************************************************************"
  print "*****************************************************************************************"
  print "*****************************************************************************************"
  for i in range(-5,6):
    print "*****************************************************************************************"
    sol = root(PAM, np.array([nf0,mf0,df0,f0f0,fuf0,fdf0,-1.,-1.,-1.,-1.,-1.,0.]),method=met,options=opt)
    print "Success:\t",sol['success']
    print sol['message']

    if sol['success']:
      mf0 = .2*i*nf0
      print " mf0= %.5f"%mf0
      print ""
    
      nf  = sol['x'][0]
      mf  = sol['x'][1]
      df  = sol['x'][2]
      f0f = sol['x'][3]
      fuf = sol['x'][4]
      fdf = sol['x'][5]
      m   = PAM.m(sol['x'])
      mu  = sol['x'][11]
      mc=m-mf
      nc=n-nf
      print "  nf= %.5f"%nf,"\t  nc= %.5f"%nc,"\t  mf= %.5f"%mf,"\tmc= %.5f"%mc,"\tm= %.5f"%m
      print " df2= %.5f"%df**2
      print "Df0f= %.5f"%np.power(f0f**2-(2-2*nf+2*df*df),2),
      print "\tDfuf= %.5f"%np.power(fuf**2-(nf+mf-2*df*df),2),"\tDfdf= %.5f"%np.power(fdf**2-(nf-mf-2*df*df),2)
      print "  Dn= %.5f"%np.power(n - np.abs(nc) - np.abs(nf),2)
      print "  mu= %.5f"%mu
      print PAM.Landau(sol['x'],mu='')
