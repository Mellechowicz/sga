#!/usr/bin/python
import numpy as np
from scipy import linalg as la
import qs
import sys
#  import matplotlib.pyplot as plt
#  import matplotlib as mpl
#  from matplotlib import gridspec

def err(*args):
  for a in args:
    sys.stderr.write(str(a)+' ')
  sys.stderr.write('\n')

class DispRel:
  ek = np.array([])
  def __init__(self, *args, **kwargs):
    # ARGS: hoppingi
    if(len(args)>0):
      if( type(args[0]) == type(np.array([]))):
        self.ts=args[0]
      elif(type(args[0]) == type([])):
        self.body=np.array(args[0])
      else:
        self.body = np.array(args)
    else:
      self.body = None
    # KWARGS, 'eps':	funkcja wyliczajaca relacje dyspersji
    #         'BZone':	array-like 1ej strefy Brillouina w prz. odwrotnej w postaci [ped0,ped1...]
    if 'eps' not in kwargs:
      self.eps=None
    else:
      self.eps = kwargs['eps']
    if 't' not in kwargs:
      self.ts=None
    else:
      self.ts = kwargs['t']
    if 'BZone' not in kwargs:
      self.BZone=None
    else:
      self.BZone = kwargs['BZone']

  def __call__(self,**kwargs):
    if self.eps is None:
      raise TypeError("DispRel: dispersion relation not defined!")
    if self.ts is None:
      err("DispRel: hoppings are not defined! Taking np.ones()")
      self.ts=np.ones(20) # wsp. szwagra: TODO: zmienic na inteligentne wykrywanie ile trzeba
    if self.BZone is None:
      raise TypeError("DispRel: 1st Brillouin zone not defined!")

    self.ek=self.eps(self.ts,self.BZone)
    if 'out' not in kwargs:
      return self.ek 
    else:
      if kwargs['out']=='stdout' or \
         kwargs['out']=='std' or \
         kwargs['out']=='out' or \
         kwargs['out']=='output' or \
         kwargs['out']=='':
        print self
      else:
        with open(kwargs['out'], 'w+') as f:
          f.write(str(self)+"\n")

  def __str__(self):
    if len(self.ek) != len(self.BZone):
      raise TypeError("DispRel: Resultant array is of the wrong dimension: try executing self()")
    rtrn=""
    rmb=self.BZone[0]
    for i,(k,e) in enumerate(zip(self.BZone,self.ek)):
      if np.all(k!=rmb):
        rtrn+="\n"
      rmb = k
      for j in k:
        rtrn+=str(j)+" " 
      rtrn+="%.12f\n"%(e)
    return rtrn[:-1]

 
class FreeElectron:
  num_fields = 12
  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11
  # params = np.array([0.,0.,0.,0.,0.])
  #                    n  ef V  Uf  h 

  def __init__(self,*args,**kwargs):
    if 'beta' not in kwargs: # odwrotnosc temperatury
      self.beta=100.
    else:
      self.beta = kwargs['beta']

    if 'params' in kwargs:
      N=len(kwargs['params'])
      if N > 4:
        self.n  = kwargs['params'][0]
        self.ef = kwargs['params'][1]
        self.V  = kwargs['params'][2]
        self.Uf = kwargs['params'][3]
        self.h  = kwargs['params'][4]
      elif N > 3:
        self.n  = kwargs['params'][0]
        self.ef = kwargs['params'][1]
        self.V  = kwargs['params'][2]
        self.Uf = kwargs['params'][3]
        self.h  =  0.0
      elif N > 2:
        self.n  = kwargs['params'][0]
        self.ef = kwargs['params'][1]
        self.V  = kwargs['params'][2]
        self.Uf =  5.0 
        self.h  =  0.0
      elif N > 1:
        self.n  = kwargs['params'][0]
        self.ef = kwargs['params'][1]
        self.V  = -0.5
        self.Uf =  5.0
        self.h  =  0.0
      elif N > 0:
        self.n  = kwargs['params'][0]
        self.ef = -3.0 
        self.V  = -0.5
        self.Uf =  5.0
        self.h  =  0.0
      else:
        self.n  =  1.5
        self.ef = -3.0
        self.V  = -0.5
        self.Uf =  5.0
        self.h  =  0.0
      err("Parameters set to:")
      err("n:\t",self.n)
      err("ef:\t",self.ef)
      err("V:\t",self.V)
      err("Uf:\t",self.Uf)
      err("h:\t",self.h)
    else:
      if 'n' not in kwargs: # liczba wszystkich elektronow
        self.n=1.5 # cwiartkowe obsadzenie
      else:
        self.n = kwargs['n']
      if 'ef' not in kwargs: # poziom f elektronow
        self.ef=-1.
      else:
        self.ef = kwargs['ef']
      if 'V' not in kwargs: # hybrydyzacja c-f
        self.V=0.
      else:
        self.V = kwargs['V']
      if 'Uf' not in kwargs: # odpychanie na f elektronach
        self.Uf=0.
      else:
        self.Uf = kwargs['Uf']
      if 'h' not in kwargs: # zredukowane pole magnetyczne
        self.h=0.
      else:
        self.h = kwargs['h']
      err("Parameters set to:")
      err("n:\t",self.n)
      err("ef:\t",self.ef)
      err("V:\t",self.V)
      err("Uf:\t",self.Uf)
      err("h:\t",self.h)

    if 'eps' not in kwargs: # relacja dyspersji; powinna byc klasy DispRel
      self.eps=None
    else:
      self.eps = kwargs['eps']
      self.ek = self.eps()

  def eps(eps0):
    self.eps=eps0
    self.ek = self.eps()

  def OneEnergy(self,fields,spin,eig=1):
    if not isinstance(fields,np.ndarray):
      raise TypeError("FreeElectron: Can not calculate energy: Argument is not array-like!")
    if len(fields) < self.num_fields:
      raise TypeError("FreeElectron: Can not calculate energu: Too few arguments!")

  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    if(spin>0):
      qf = qs.SqUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
      return .5*(self.ef + self.ek \
	     - 2*fields[11] - fields[9]\
             - 2*self.h - fields[10] \
             - eig*np.sqrt(np.power( \
             (self.ef - fields[9] - fields[10]) - self.ek,2)  + 4*qf*qf*self.V*self.V))

    if(spin<0):
      qf = qs.SqDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
      return .5*(self.ef + self.ek \
	     - 2*fields[11] -  fields[9]\
             + 2*self.h + fields[10] \
             - eig*np.sqrt(np.power( \
             (self.ef - fields[9] + fields[10]) - self.ek,2)  + 4*qf*qf*self.V*self.V))
 
  def logexp(self,E):
    if E < 0:
      return E-np.log(1.+np.exp(self.beta*E))/self.beta
    else:
      return -np.log(1.+np.exp(-self.beta*E))/self.beta
  nplogexp = np.vectorize(logexp)

  def FDdist(self,E):
    result = .5*(1.-np.tanh(.5*self.beta*E))

####################################################################################################
####res = np.resize(result,(int(np.sqrt(len(E))),int(np.sqrt(len(E)))))
####en = np.resize(E,(int(np.sqrt(len(E))),int(np.sqrt(len(E)))))
####fig = plt.figure()
####plt.rc('text', usetex=True)
####plt.rc('font', family='serif')
####gs = gridspec.GridSpec(2, 2)
####ax1 = plt.subplot(gs[0,0])
####plt.pcolor(en)
####plt.colorbar()
####plt.title("Energy")
####ax2 = plt.subplot(gs[0,1])
####plt.pcolor(np.resize(self.ek,(int(np.sqrt(len(E))),int(np.sqrt(len(E))))))
####plt.colorbar()
####plt.title("Dispersion")
####ax3 = plt.subplot(gs[1,0])
####plt.pcolor(res)
####plt.colorbar()
####plt.title("Distribution")
####ax4 = plt.subplot(gs[1,1])
####plt.text(0.5, 0.5, str(np.sum(result)/len(E)), fontsize=16, ha='center')
####plt.show()
####################################################################################################

    return result 
  def Landau(self,fields,**kwargs):
    if not isinstance(fields,np.ndarray):
      raise TypeError("FreeElectron: Can not calculate energy: Argument is not array-like!")
    if len(fields) < self.num_fields:
      raise TypeError("FreeElectron: Can not calculate energu: Too few arguments!")

  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    Ep = self.OneEnergy(fields, 1)
    Em = self.OneEnergy(fields,-1)
    Ep2= self.OneEnergy(fields, 1,-1)
    Em2= self.OneEnergy(fields,-1,-1)

    Lambda = len(Ep)

    Omega =  (
	+ fields[9]*fields[0] + fields[10]*fields[1]	# lnf*nf + lmf*mf
	+ self.Uf*fields[2]*fields[2]			# Uf*df**2
	+ fields[6]*(fields[3]*fields[3] - 2. + 2*fields[0] - 2*fields[2]*fields[2]) 	  # zachowanie f0f
	+ fields[7]*(fields[4]*fields[4] - fields[0] - fields[1] + 2*fields[2]*fields[2])# zachowanie fuf
	+ fields[8]*(fields[5]*fields[5] - fields[0] + fields[1] + 2*fields[2]*fields[2])# zachowanie fdf
	)

    if 'mu' in kwargs:
      Omega += fields[11]*self.n # mu*n

    Omega += np.sum(self.nplogexp(self,Ep)+self.nplogexp(self,Em)+self.nplogexp(self,Ep2)+self.nplogexp(self,Em2))/Lambda # calka po strefie Brillouina

    return Omega/self.n

  def __call__(self,fields,**kwargs):
    if not isinstance(fields,np.ndarray):
      raise TypeError("FreeElectron: Can not calculate energy: Argument is not array-like!")
    if len(fields) < self.num_fields:
      raise TypeError("FreeElectron: Can not calculate energu: Too few arguments!")


  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    Ep = self.OneEnergy(fields, 1)
    Em = self.OneEnergy(fields,-1)
    Ep2= self.OneEnergy(fields, 1,-1)
    Em2= self.OneEnergy(fields,-1,-1)

    Lambda = len(Ep)

    qfUP = qs.SqUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
    qfDO = qs.SqDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])

    # Macierze jak w notatkach:
    Du = self.FDdist(Ep)
    Dd = self.FDdist(Em)
    Du2 = self.FDdist(Ep2)
    Dd2 = self.FDdist(Em2)
    Lu = (self.ef - fields[9] - fields[10]) - self.ek 
    Mu = 1./(np.sqrt(np.power(Lu,2)  + 4*qfUP*qfUP*self.V*self.V))
    Lu = Lu*Mu

    Ld = (self.ef - fields[9] + fields[10]) - self.ek
    Md = 1./(np.sqrt(np.power(Ld,2)  + 4*qfDO*qfDO*self.V*self.V))
    Ld = Ld*Md

    Up = 0.5*np.sum(Du*(Lu-1.)-Du2*(Lu+1.))/Lambda
    Do = 0.5*np.sum(Dd*(Ld-1.)-Dd2*(Ld+1.))/Lambda

    Xn = np.sum(Du+Du2+Dd+Dd2)/Lambda
    Xlnf= (Up + Do) 
    Xlmf= (Up - Do) 
    Xuf= -2.*qfUP*self.V*self.V*np.sum((Du-Du2)*Mu)/Lambda
    Xdf= -2.*qfDO*self.V*self.V*np.sum((Dd-Dd2)*Md)/Lambda

  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    SetEqs = np.array([
	fields[9] + 2*fields[6] - fields[7] - fields[8] + Xuf*qs.SqUPdn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	fields[10] - fields[7] + fields[8] + Xuf*qs.SqUPdm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	2*fields[2]*(self.Uf + 2*(-fields[6] + fields[7] + fields[8])) + Xuf*qs.SqUPdd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	2*fields[3]*fields[6] + Xuf*qs.SqUPdf0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdf0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	2*fields[4]*fields[7] + Xuf*qs.SqUPdfu(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdfu(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	2*fields[5]*fields[8] + Xuf*qs.SqUPdfd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdfd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]),
	fields[3]*fields[3] - 2. + 2*fields[0] - 2*fields[2]*fields[2],
	fields[4]*fields[4] - fields[0] - fields[1] + 2*fields[2]*fields[2],
	fields[5]*fields[5] - fields[0] + fields[1] + 2*fields[2]*fields[2],
        fields[0] + Xlnf,
        fields[1] + Xlmf,
        self.n - Xn
    ])

    return SetEqs

  def m(self,fields,**kwargs):
    if not isinstance(fields,np.ndarray):
      raise TypeError("FreeElectron: Can not calculate energy: Argument is not array-like!")
    if len(fields) < self.num_fields:
      raise TypeError("FreeElectron: Can not calculate energu: Too few arguments!")

  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    Ep = self.OneEnergy(fields, 1)
    Em = self.OneEnergy(fields,-1)
    Ep2= self.OneEnergy(fields, 1,-1)
    Em2= self.OneEnergy(fields,-1,-1)

    Lambda = float(len(Ep))

    qfUP = qs.SqUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
    qfDO = qs.SqDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])

    # Macierze jak w notatkach:
    Du = self.FDdist(Ep)
    Dd = self.FDdist(Em)
    Du2 = self.FDdist(Ep2)
    Dd2 = self.FDdist(Em2)

    return np.sum(Du+Du2-Dd-Dd2)/Lambda

  def err(self,fields,**kwargs):
    if not isinstance(fields,np.ndarray):
      raise TypeError("FreeElectron: Can not calculate energy: Argument is not array-like!")
    if len(fields) < self.num_fields:
      raise TypeError("FreeElectron: Can not calculate energu: Too few arguments!")

  # fields = nf mf df f0f fuf fdf l0f luf ldf lnf lmf mu
  #           0  1  2   3   4   5   6   7   8   9  10 11

    Ep = self.OneEnergy(fields, 1)
    Em = self.OneEnergy(fields,-1)
    Ep2= self.OneEnergy(fields, 1,-1)
    Em2= self.OneEnergy(fields,-1,-1)

    Lambda = float(len(Ep))
    Lam = int(np.sqrt(Lambda))

    qfUP = qs.SqUP(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])
    qfDO = qs.SqDO(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5])

    # Macierze jak w notatkach:
    Du = self.FDdist(Ep)
    Dd = self.FDdist(Em)
    Du2 = self.FDdist(Ep2)
    Dd2 = self.FDdist(Em2)
####################################################################################################
####fig = plt.figure()
####plt.rc('text', usetex=True)
####plt.rc('font', family='serif')
####gs = gridspec.GridSpec(2, 2)
####ax1 = plt.subplot(gs[0,0])
####plt.pcolor(np.resize(Ep,(int(Lam),int(Lam))))
####plt.colorbar()
####plt.title("Energy 1")
####ax2 = plt.subplot(gs[0,1])
####plt.pcolor(np.resize(Ep2,(int(Lam),int(Lam))))
####plt.colorbar()
####plt.title("Energy 2")
####ax3 = plt.subplot(gs[1,0])
####plt.pcolor(np.resize(Du,(int(Lam),int(Lam))))
####plt.colorbar()
####plt.title("Distribution 1")
####plt.text(0.5, 0.5, str(np.sum(Du)/Lambda), fontsize=16, ha='center')
####ax4 = plt.subplot(gs[1,1])
####plt.pcolor(np.resize(Du2,(int(Lam),int(Lam))))
####plt.colorbar()
####plt.title("Distribution 2")
####plt.text(0.5, 0.5, str(np.sum(Du2)/Lambda), fontsize=16, ha='center')
####plt.show()
####################################################################################################
  
    Lu = (self.ef - fields[9] - fields[10]) - self.ek 
    Mu = 1./(np.sqrt(np.power(Lu,2)  + 4*qfUP*qfUP*self.V*self.V))
    Lu = Lu*Mu

    Ld = (self.ef - fields[9] + fields[10]) - self.ek
    Md = 1./(np.sqrt(np.power(Ld,2)  + 4*qfDO*qfDO*self.V*self.V))
    Ld = Ld*Md

    Up = 0.5*np.sum(Du*(Lu-1.)-Du2*(Lu+1.))/Lambda
    Do = 0.5*np.sum(Dd*(Ld-1.)-Dd2*(Ld+1.))/Lambda

    Xn = np.sum(Du+Du2+Dd+Dd2)/Lambda
    Xlnf= (Up + Do) 
    Xlmf= (Up - Do) 
    Xuf= -2.*qfUP*self.V*self.V*np.sum((Du-Du2)*Mu)/Lambda
    Xdf= -2.*qfDO*self.V*self.V*np.sum((Dd-Dd2)*Md)/Lambda

    error = 0.0
    error += np.abs(fields[9] + 2*fields[6] - fields[7] - fields[8] + Xuf*qs.SqUPdn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdn(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(fields[10] - fields[7] + fields[8] + Xuf*qs.SqUPdm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdm(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(2*fields[2]*(self.Uf + 2*(-fields[6] + fields[7] + fields[8])) + Xuf*qs.SqUPdd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(2*fields[3]*fields[6] + Xuf*qs.SqUPdf0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdf0(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(2*fields[4]*fields[7] + Xuf*qs.SqUPdfu(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdfu(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(2*fields[5]*fields[8] + Xuf*qs.SqUPdfd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]) + Xdf*qs.SqDOdfd(fields[0],fields[1],fields[2],fields[3],fields[4],fields[5]))
    error += np.abs(fields[3]*fields[3] - 2. + 2*fields[0] - 2*fields[2]*fields[2])
    error += np.abs(fields[4]*fields[4] - fields[0] - fields[1] + 2*fields[2]*fields[2])
    error += np.abs(fields[5]*fields[5] - fields[0] + fields[1] + 2*fields[2]*fields[2])
    error += np.abs(fields[0] + Xlnf)
    error += np.abs(fields[1] + Xlmf)
    error += np.abs(self.n - Xn)
    
    return error

