#!/usr/bin/python

import numpy as np
import scipy.optimize as opt
import scipy.linalg as lin

# definicja nieskonczonosci
INFTY = 1e1
# defnicja "okolicy" bieguna i obszaru wyplaszczonego
NEIGH = 1e-2
FLAT = 1e-2*NEIGH
def infty(C,x,p=-0.5):
  global FLAT
  if x < FLAT:
    return C*np.power(FLAT,p)
  else:
    return C*np.power(x,p)

#################################################################################################
#################################################################################################
#################################################################################################
###
###  Madre wersje -> obsluga biegunow -> z dokladnoscia do INF
###

def SqUP(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty((np.sqrt(2)*d*fd + f0*fu)/np.sqrt(2),delta)
  else:
    if deltb < NEIGH:
      return infty((np.sqrt(2)*d*fd + f0*fu)/np.sqrt(2),deltb)

  if d <= 0.:
    return (f0*fu)/np.sqrt(delta*deltb)

  return (np.sqrt(2)*d*fd + f0*fu)/np.sqrt(delta*deltb)

def SqUPdn(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty((np.sqrt(2)*d*fd + f0*fu)/(2.*np.sqrt(2)) ,delta,-1.5)
  else:
    if deltb < NEIGH:
      return infty(-(np.sqrt(2)*d*fd + f0*fu)/(2.*np.sqrt(2)) ,deltb,-1.5)

  if d <= 0.:
    return (f0*fu*(-1 + m + n))/np.power(delta*deltb,1.5)

  return ((np.sqrt(2)*d*fd + f0*fu)*(-1 + m + n))/np.power(delta*deltb,1.5) 

def SqUPdm(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty((np.sqrt(2)*d*fd + f0*fu)/(2.*np.sqrt(2)) ,delta, -1.5)
  else:
    if deltb < NEIGH:
      return infty(-(np.sqrt(2)*d*fd + f0*fu)/(2.*np.sqrt(2)) ,deltb, -1.5)

  if d <= 0.:
    return (f0*fu*(-1 + m + n))/np.power(delta*deltb,1.5)

  return ((np.sqrt(2)*d*fd + f0*fu)*(-1 + m + n))/np.power(delta*deltb,1.5) 

def SqUPdd(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(fd ,delta)
  else:
    if deltb < NEIGH:
      return infty(fd,deltb)

  if d <= 0.:
    return (np.sqrt(2)*fd)/np.sqrt(delta*deltb)

  return (np.sqrt(2)*fd)/np.sqrt(delta*deltb)

def SqUPdf0(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(fu/(np.sqrt(2)) ,delta)
  else:
    if deltb < NEIGH:
      return infty(fu/(np.sqrt(2)) ,deltb)

  return fu/np.sqrt(delta*deltb) 

def SqUPdfu(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(f0/(np.sqrt(2)) ,delta)
  else:
    if deltb < NEIGH:
      return infty(f0/(np.sqrt(2)),deltb)

  if d <= 0.:
    return f0/np.sqrt(delta*deltb)

  return f0/np.sqrt(delta*deltb) 

def SqUPdfd(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n-m
  deltb = n+m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(d,delta)
  else:
    if deltb < NEIGH:
      return infty(d,deltb)

  if d <= 0.:
    return 0. 

  return (np.sqrt(2)*d)/np.sqrt(delta*deltb)

def SqDO(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty((np.sqrt(2)*d*fu + f0*fd)/np.sqrt(2),delta)
  else:
    if deltb < NEIGH:
      return infty((np.sqrt(2)*d*fu + f0*fd)/np.sqrt(2),deltb)

  if d <= 0.:
    return (f0*fu)/np.sqrt(delta*deltb)

  return (np.sqrt(2)*d*fu + f0*fd)/np.sqrt(delta*deltb)

def SqDOdn(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty((f0*fd + np.sqrt(2)*d*fu)/(2.*np.sqrt(2)),delta,-1.5)
  else:
    if deltb < NEIGH:
      return infty(-(f0*fd + np.sqrt(2)*d*fu)/(2.*np.sqrt(2)),deltb,-1.5)

  if d <= 0.:
    return -((f0*fd*(1 + m - n))/np.power(delta*deltb,1.5))

  return -(((f0*fd + np.sqrt(2)*d*fu)*(1 + m - n))/np.power(delta*deltb,1.5)) 

def SqDOdm(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(-(f0*fd + np.sqrt(2)*d*fu)/(2.*np.sqrt(2)),deltb,-1.5)
  else:
    if deltb < NEIGH:
      return infty((f0*fd + np.sqrt(2)*d*fu)/(2.*np.sqrt(2)),deltb,-1.5)

  if d <= 0.:
    return (f0*fd*(1 + m - n))/np.power(delta*deltb,1.5)

  return ((f0*fd + np.sqrt(2)*d*fu)*(1 + m - n))/np.power(delta*deltb,1.5) 

def SqDOdd(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(fu ,delta)
  else:
    if deltb < NEIGH:
      return infty(fu ,deltb)

  if d <= 0.:
    return (np.sqrt(2)*fu)/np.sqrt(delta*deltb)

  return (np.sqrt(2)*fu)/np.sqrt(delta*deltb)

def SqDOdf0(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(fd/np.sqrt(2) ,delta)
  else:
    if deltb < NEIGH:
      return infty(fd/np.sqrt(2) ,deltb)

  return fd/np.sqrt(delta*deltb) 

def SqDOdfu(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(d ,delta)
  else:
    if deltb < NEIGH:
      return infty(d ,deltb)

  if d <= 0.:
    return 0.

  return (np.sqrt(2)*d)/np.sqrt(delta*deltb) 

def SqDOdfd(n,m,d,f0=-1.,fu=-1.,fd=-1.): # Jesli pola-duchy sa ujemne wylicza je z definicji
  global NEIGH
  delta = 2.-n+m
  deltb = n-m
  if delta*deltb <= 0:
    return 0.

  if n >= 1.:
    if delta < NEIGH:
      return infty(f0/np.sqrt(2) ,delta)
  else:
    if deltb < NEIGH:
      return infty(f0/np.sqrt(2) ,deltb)

  return f0/np.sqrt(delta*deltb)

