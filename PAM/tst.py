#!/usr/local/bin/python

import numpy as np
import SOCenergies as SOC

ens = SOC.SOC_energy(SOC=0.0+0.J,h=0.0+0.J)
fs = [-3.,1.,1.,0.1,0.+0.j]

print ens
print ens.sp_energy1(*fs)
print ens.sp_energy2(*fs)
print ens.sp_energy3(*fs)
print ens.sp_energy4(*fs)
