#!/usr/bin/python

import PAM.qs as qs
import numpy as np

delta=0.0025
n0=.99999
ds = np.arange(0., .5, delta)
mMax = n0
if n0 > 1.:
  mMax = 2-n0
ms = np.arange(-mMax, mMax+.5*delta, delta)

#F=[qs.SqUP,qs.SqUPdn,qs.SqUPdm,qs.SqUPdd,qs.SqUPdf0,qs.SqUPdfu,qs.SqUPdfd]
F=[qs.SqDO,qs.SqDOdn,qs.SqDOdm,qs.SqDOdd,qs.SqDOdf0,qs.SqDOdfu,qs.SqDOdfd]

for m in ms:
  for d in ds:
    print m,d,
    for f in F:
      print f(n0,m,d,0.01,0.1,0.1),
    print ""
  print "" 
