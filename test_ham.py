#!/usr/bin/python

import PAM.hamiltonian as ham
import numpy as np
from scipy.optimize import root

Lambda=1e3
kxMax = np.pi
kyMax = np.pi
kxs = np.linspace(-kxMax,kxMax,Lambda)
kys = np.linspace(-kyMax,kyMax,Lambda)
kxxs,kyys = np.meshgrid(kxs,kys)

ks = np.stack((np.ravel(kxxs), np.ravel(kyys)), axis=-1)

def eps(ts,ks): # SC lattice with only NN hopping
  return 2*ts[0]*(np.cos(ks[:,0])+np.cos(ks[:,1]))

n  = 1.8 
ef = -1.
V  = -0.1
h  = 0.
Uf = 5.
Uc = 0.1
beta = 1000.

epsSC = ham.DispRel(eps=eps,BZone=ks,t=np.array([-1.0]))
PAM = ham.FreeElectron(params=[n,ef,V,h,Uf,Uc],beta=beta,eps=epsSC)

nf = np.abs(2.-n)
nc = n - nf
m = 0.
mf = 0.
mc = m - mf
dc = .01 
df = .0
f0c = np.sqrt(np.abs(2-2*nc))
f0f = np.sqrt(np.abs(2-2*nf))
fuc = np.sqrt(np.abs(nc-mc))
fuf = np.sqrt(np.abs(nf-mf))
fdc = np.sqrt(np.abs(nc+mc))
fdf = np.sqrt(np.abs(nf+mf))

#met = 'hybr'
met = 'lm'
#met = 'broyden1'
#met = 'broyden2'
#met = 'anderson'
#met = 'linearmixing'
#met = 'diagbroyden'
#met = 'excitingmixing'
#met = 'krylov'
#met = 'df-sane'


sol = root(PAM, np.array([0.,0.1,nf,m,0.1,mf,dc,df,f0c,f0f,fuc,fuf,fdc,fdf,-0.1,-0.1,-0.1,-0.1,-0.1,-0.1,0.1,0.1,-0.1]),method=met,tol=1e-16)
mu = sol['x'][0]
nf = sol['x'][2]
m  = sol['x'][3]
mf = sol['x'][5]
dc = sol['x'][6]
df = sol['x'][7]
f0c = sol['x'][8]
f0f = sol['x'][9]
fuc = sol['x'][10]
fuf = sol['x'][11]
fdc = sol['x'][12]
fdf = sol['x'][13]
mc=m-mf
nc=n-nf
print "  nf= %.3f"%nf,"\t  nc= %.3f"%(n-nf),"\t  mf= %.3f"%mf,"\tmc= %.3f"%(m-mf),"\tm= %.3f"%m
print " df2= %.3f"%df**2,"\t dc2= %.3f"%dc**2
print "Df0c= %.3f"%np.power(f0c-np.sqrt(2-2*nc+2*dc*dc),2),
print "\tDfuc= %.3f"%np.power(fuc-np.sqrt(nc+mc-2*dc*dc),2),"\tDfdc= %.3f"%np.power(fdc-np.sqrt(nc-mc-2*dc*dc),2)
print "Df0f= %.3f"%np.power(f0f-np.sqrt(2-2*nf+2*df*df),2),
print "\tDfuf= %.3f"%np.power(fuf-np.sqrt(nf+mf-2*df*df),2),"\tDfdf= %.3f"%np.power(fdf-np.sqrt(nf-mf-2*df*df),2)
print PAM.Landau(sol['x'],mu='')
